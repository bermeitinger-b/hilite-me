# syntax=docker/dockerfile:1.4
FROM python:3.11-slim AS base

LABEL maintainer="Bernhard Bermeitinger <bernhard.bermeitinger@unisg.ch>"

# Base Setup
SHELL ["/bin/bash", "-e", "-o", "pipefail", "-c"]


RUN \
  --mount=type=cache,sharing=locked,target=/var/cache/apt \
  --mount=type=cache,sharing=locked,target=/var/lib/apt \
  <<EOF

    # Setup
    export DEBIAN_FRONTEND="noninteractive"
    echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
    rm -f /etc/apt/apt.conf.d/docker-clean
    echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/keep-cache


    # Install dependencies
    apt-get update -q
    apt-get install -y --no-install-recommends \
        "locales" \
        "tzdata"

    echo "en_US.UTF-8 UTF-8" > /etc/locale.gen

    locale-gen

    useradd \
        --create-home \
        --no-log-init \
        --shell "/bin/bash" \
        --groups users \
        "user"
EOF

ENV LC_ALL="en_US.UTF-8"
ENV LANG="en_US.UTF-8"
ENV LANGUAGE="en_US.UTF-8"

FROM base AS poetry
RUN \
    --mount=type=cache,sharing=locked,target=/var/cache/apt \
    --mount=type=cache,sharing=locked,target=/var/lib/apt \
    <<EOF
    apt-get update -q
    apt-get install -y --no-install-recommends \
        "curl"

EOF
 
USER user
WORKDIR /home/user
ENV POETRY_HOME="/home/user/poetry"
ENV POETRY_VIRTUALENVS_IN_PROJECT="false"
ENV POETRY_NO_INTERACTION="true"
ENV PATH="${POETRY_HOME}/bin:${PATH}"

RUN \
    <<EOF

    # Install poetry
    curl -fsSL --compressed "http://install.python-poetry.org" | python -
EOF

FROM poetry AS deps
COPY "./poetry.lock" "./pyproject.toml" "/home/user"
RUN \
    --mount=type=cache,uid=1000,target="/home/user/.cache/pypoetry/artifacts" \
    --mount=type=cache,uid=1000,target="/home/user/.cache/pypoetry/cache" \
    --mount=type=cache,uid=1000,target="/home/user/.cache/pip" \
    <<EOF
 
    # Use poetry to create the requirements.txt
    poetry export \
        --without=dev \
        --format=requirements.txt \
        --output=requirements.txt

    # Use pip to build all wheels
    pip wheel \
        --verbose \
        --wheel-dir "/home/user/wheels" \
        --require-hashes \
        -r requirements.txt
    
    # Done.
EOF

FROM base AS final
USER user
ENV PATH="/home/user/.local/bin:${PATH}"
RUN \
    --mount=type=cache,uid=1000,gid=1000,target="/home/user/.cache/pip" \
    --mount=type=bind,from=deps,source="/home/user/wheels",target="/home/user/wheels" \
    <<EOF

    # Install all pre-built wheels.
    find /home/user/wheels -iname "*.whl" | \
        xargs pip install --user

    # Done.
EOF

WORKDIR /home/user
ADD hilite_me "/home/user/hilite_me"
CMD ["hypercorn", "hilite_me.hiliteme:app", "-b", "0.0.0.0:8000", "--log-level", "ERROR"]
